// $Id: useTimestamps.js 7816 2015-01-07 16:06:10Z marco $

var crypto = require('crypto');

exports.useTimestamps = function (schema, options) {
        
    // Add extra field to schema...
    schema.add({created_at: Date, modified_at: Date, etag: String});
    
    schema.pre('save', function (next) {
    
        var self= this,
            content= self.toObject(),
            excludePaths= ['_id', 'modified_at', 'etag', '__v'];
        
        function generateEtag(content) {

            var etag= "",
                hash = crypto.createHash('md5');
            
            // Do some cleanup...
            excludePaths.forEach(function(path){                
                delete content[path];               
            });
            
            if (Object.keys(content).length > 0){
                hash.update(JSON.stringify(content));
                etag= hash.digest('hex');
            }

            return etag;
        };
        
        if (self.isNew) {
            self.created_at = self.modified_at = new Date;
            self.etag= generateEtag(content);
        } 
        else {
            if (self.modifiedPaths().length>0) {
                self.modified_at = new Date;
                self.etag= generateEtag(content);
            }
        }
    
        next();
    }); 
};