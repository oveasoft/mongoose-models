{
  "name": "mongoose-models",
  "version": "1.2.0",
  "description": "A extension to mongoose's models",
  "main": "lib/index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git://github.com/SportZing/mongoose-models"
  },
  "keywords": [
    "mongoose",
    "mongo",
    "db",
    "model"
  ],
  "author": {
    "name": "James Brumond"
  },
  "license": "MIT",
  "dependencies": {
    "mongoose": "6.5.x",
    "wrench": "~1.3.9",
    "mongoose-types": "~1.0.3",
    "uuid-v4": "~0.1.0",
    "colors": "0.6.0-1"
  },
  "readme": "# mongoose-models\n\nAn extension of Mongoose's models\n\n## Install\n\n```bash\n$ npm install mongoose-models\n```\n\n## Init\n\n```javascript\nrequire('mongoose-models').init({\n\turl: 'mongodb://localhost/dbname',\n\ttypes: [ 'email', 'url', 'uuid' ],\n\tmodelPath: '/path/to/models/dir'\n});\n```\n\n## Usage\n\n##### models/Person.js\n\n```javascript\nvar models = require('mongoose-models');\n\nvar Person = models.create('Person', {\n\t\n\t// If this is given and truthy, the mongoose-types timestamps\n\t// plugin will be loaded for this model creating automatically\n\t// updating 'createdAt' and 'updatedAt' properties\n\tuseTimestamps: true,\n\t\n\t// Define your mongoose schema here\n\tschema: {\n\t\tfirstName: String,\n\t\tlastName: String,\n\t\t\n\t\t// Special types like Email, Url, and ObjectId can be accessed\n\t\t// through the models.types object\n\t\temail: models.types.Email,\n\t\twebsite: models.types.Url\n\t},\n\t\n\t// Instance methods can be defined here, eg.\n\t//  \n\t//  Person.findOne({ firstName: 'bob' }, function(err, bob) {\n\t//    bob.sendEmail(...);\n\t//  });\n\t//\n\tmethods: {\n\t\t\n\t\tsendEmail: function(subject, msg) {\n\t\t\tsomeMailingLib.sendEmail(this.email, subject, msg);\n\t\t}\n\t\t\n\t},\n\t\n\t// Anything other than the above properties is considered a static\n\t// properties and stored directly on the model, eg.\n\t//\n\t//  Person.findByName('bob', function(err, bob) {\n\t//    ...\n\t//  });\n\t//\n\tfindByName: function(name, callback) {\n\t\tname = name.split(' ');\n\t\tvar lookup = { firstName: name[0] };\n\t\tif (name.length > 1) {\n\t\t\tlookup.lastName = name.pop();\n\t\t}\n\t\tPerson.findOne(lookup, callback);\n\t}\n\t\n});\n```\n\n##### some-other-file.js\n\n```javascript\nvar models = require('mongoose-models');\n\nvar Person = models.require('Person')();\n\nPerson.findByName('bob', function(err, bob) {\n\t\n});\n```\n\n### Circular References\n\nCircular references are rather messy in Mongoose. To make this much easier there is built-in support for circular references in mongoose-models. For example, say you have two models:\n\n##### Foo.js\n\n```javascript\nvar models = require('mongoose-models');\n\nvar Bar = models.require('Bar')();\n\nmodels.create('Foo', {\n\tschema: {\n\t\tbar: { type: models.types.ObjectId, ref: Bar }\n\t}\n});\n```\n\n##### Bar.js\n\n```javascript\nvar models = require('mongoose-models');\n\nvar Foo = models.require('Foo')();\n\nmodels.create('Bar', {\n\tschema: {\n\t\tfoo: { type: models.types.ObjectId, ref: Foo }\n\t}\n});\n```\n\nThis doesn't work because the models are trying to reference each other before they have been created. To make this work, we change the `ref` value like so in both files:\n\n```javascript\n{\n\tbar: { type: models.types.ObjectId, ref: {$circular: 'Bar'} }\n}\n```\n\n```javascript\n{\n\tfoo: { type: models.types.ObjectId, ref: {$circular: 'Foo'} }\n}\n```\n\nNow everything works as expected. There is also a shorter version of this if a model needs to reference itself recursively.\n\n```javascript\nvar models = require('mongoose-models');\n\nmodels.create('Baz', {\n\tschema: {\n\t\tchild: { type: models.types.ObjectId, ref: '$circular' }\n\t}\n});\n```\n\n### models.require(...)\n\nThe `models.require` method that loads models does not return the model directly, but instead returns a function that can be used to fetch the model. This is so that when two models make use of each other, the models are allowed time to set themselves up. That is why models are loaded as `models.require(...)()`. This returned function has a number of properties on it that can be used as well.\n\n#### models.require(...).model\n\nThe model function itself (once defined). This is the same as what is returned from the getter function.\n\n#### models.require(...).schema\n\nThe mongoose schema object. This can be accessed immediately without waiting for the model to be created if you need access sooner. This could be used as an alternative to the `$circular` syntax described above.\n\n#### models.require(...).resolve( callback )\n\nDefines a callback to be run once the model has been created.\n\n```javascript\nvar Foo = models.require('Foo');\nFoo.resolve(function() { Foo = Foo.model; });\n```\n\n### Debugging REPL\n\nThe REPL is a simple JavaScript interpreter with access to your mongoose-models. Before using the REPL, it will need to be loaded and configured.\n\n```bash\n$ cp ./node_modules/mongoose-models/bin/repl.js ./repl.js\n```\n\nNow, open up `repl.js` and change the values in `conf` to match your configuration settings. You can start the REPL by running:\n\n```bash\n$ node repl.js\n```\n\nThe REPL comes with some helpful features on top of the standard node REPL. First, mongoose-models is already loaded for you and is available as `models`. Second, `models.require` has been patched to automatically store loaded models in `global`. There are also some useful functions defined.\n\n```javascript\nLoading REPL...\n> models.require('Foo');\nundefined\n> Foo.find({ }, store('foos'));\n{ ... }\n> \nStored 2 arguments in \"foos\"\n> print(foos);\n{\n  '1': ...\n  '2': ...\n  ...\n}\n```\n\n",
  "readmeFilename": "readme.md",
  "_id": "mongoose-models@0.3.1",
  "dist": {
    "shasum": "f152b042148e2e7aca1dd7aa56de04a6269729a7",
    "tarball": "http://registry.npmjs.org/mongoose-models/-/mongoose-models-0.3.1.tgz"
  },
  "_npmVersion": "1.1.65",
  "_npmUser": {
    "name": "tauren",
    "email": "tauren@sportzing.com"
  },
  "maintainers": [
    {
      "name": "k",
      "email": "kbjr14@gmail.com"
    },
    {
      "name": "tauren",
      "email": "tauren@sportzing.com"
    }
  ],
  "directories": {},
  "_shasum": "f152b042148e2e7aca1dd7aa56de04a6269729a7",
  "_resolved": "https://registry.npmjs.org/mongoose-models/-/mongoose-models-0.3.1.tgz",
  "_from": "mongoose-models@>=0.3.0 <0.4.0"
}
