var fs          = require('fs');
var path        = require('path');
var mongoose    = require('mongoose');
var extend      = require('node.extend');
var _           = require('lodash');
var keywordize  = require('mongoose-keywordize');

// Expose mongoose and mongoose's types
exports.mongoose    = mongoose; 
exports.types       = mongoose.SchemaTypes;
    
exports.init = function(conf, dependencies) {

    let connection = mongoose.createConnection(conf.url),
        commonSchema = require(path.join(conf.schemaPath, 'common.js')),       
        auditLog= conf.logger,
        schemas = {};        
        
    mongoose.set('debug', conf.debug || false);
    console.log('####### mongoose version: %s #######', mongoose.version);  
    
    // Expose schemas repo in outside world...
    exports.schemaRepository = schemas;
    exports.db= connection;
    exports.registerPlugins= loadSchemaPlugin;
    exports.registerDomains= extendSchemaStatics;
    
    connection.on('open', function(){
console.log('--------------> connection::opened');
    });
    
    connection.on('err', function(err){
console.log('--------------> connection::err=', err);
        process.exit(1);
    });
    
    // Add mongoose transport to the logging system if needed...
    if (auditLog)
        auditLog.addTransport("mongoose", mongoose.Schema, {mongoConnection: connection, collectionName: 'audit.logs'});

    /*----------------------------------------------*/
    function loadSchemaRegistry(modelName, filename){
    
        let schema= {};
    
        modelName = modelName.replace('.js', '');
    
        if (fs.existsSync(filename)) {
            
            schema= require(filename);          
            schemas[modelName]= {
                path: filename,
                collection: schema.collection || '',
                paths: schema.paths || {},
                schema: schema.definition || {},                
                funcs: extend(schema.funcs || {}, commonSchema.funcs),
                globals: commonSchema.globals || {},
                options: extend(schema.options || {}, commonSchema.options),
                columns: schema.columns || {}
            };
        }else{
            schemas[modelName]= {options: {}};  
        }            
    }
    
    /*----------------------------------------------*/
    function loadDomainRegistry(schema, filename){
        if (fs.existsSync(filename)) {
            domain= require(filename);          
            extend(schema.statics, domain);
        }
    }

    /*----------------------------------------------*/
    function loadSchemaPlugin(name, schema) {
        
        var plugins= require(path.join(conf.schemaPath, 'plugins.js'))(dependencies),
            options= schemas[name].options;
            
        _.forIn(options, function(settings, opt){       
            switch (opt.toLowerCase()) {                                        
                case "usekeyword":
                    if (typeof settings === 'object')
                        schema.plugin(keywordize, settings);
                break;
                
                case "useaudit":                
                    if (auditLog && typeof settings === 'object'){                              
                        var pluginFn = auditLog.getPlugin('mongoose', settings); // setup occurs here
                        schema.plugin(pluginFn.handler); // .handler is the pluggable function for mongoose in this case
                    }
                break;
                
                case "useelastic":
                    if (typeof settings === 'object')
                        schema.plugin(plugins.elasticsearch, settings);
                break;
                
                case "usemessaging":
                    if (typeof settings === 'object')
                        schema.plugin(plugins.rabbitmessaging, settings);
                break;
            }           
        });
        
        // Set paginate stuff on all schemas...
        schema.plugin(plugins.paginate, {name: name, repo: schemas[name]});
        schema.plugin(plugins.customPaginate, {name: name, repo: schemas[name]});
		schema.plugin(plugins.join, {name: name, repo: schemas[name]});
        schema.plugin(plugins.customJoin, {name: name, repo: schemas[name]})        
    }
    
    /*----------------------------------------------*/
    function extendSchemaStatics(name, schema){

        var domainsPaths= path.join(conf.domainPath, name),
            files= fs.readdirSync(domainsPaths);

        _.each(files, function(file){
            loadDomainRegistry(schema, path.join(conf.domainPath, name,  file));
        });     
    }

    /*----------------------------------------------*/
    function loadModelRegistry(modelName, modelFile){
    
        modelName = modelName.replace('.js', '');
        
        var schema= new mongoose.Schema(schemas[modelName].schema);     
        require(modelFile)(connection, modelName, schema);      
    }
    
    var files= fs.readdirSync(conf.schemaPath).sort();
    _.each(files, function(file){
        loadSchemaRegistry(file, path.join(conf.schemaPath, file));
        //console.log('load schema file %s', file);
    });

    files= fs.readdirSync(conf.modelPath).sort();
    _.each(files, function(file){
        loadModelRegistry(file, path.join(conf.modelPath, file));
        //console.log('load model file %s', file);
    });

    //Expose specific clients in models world...    
    exports.mailClient = dependencies.mailClient;
    exports.v1Client = dependencies.v1Client;
    exports.mysqlClient = dependencies.mysqlClient;
    exports.carformClient = dependencies.carformClient;
    
    // Don't allow re-init
    exports.init = undefined;
};